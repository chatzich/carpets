import React from 'react'
import { Row, Col, Container, Button, Form } from 'react-bootstrap';
import { deliverCarpet, getParameters, deleteCarpet, getCarpetsByCustomerId, getCustomerById} from './UtilityFunctions'
import dateformat from "dateformat";

class CarpetDeliveryTable extends React.Component {
    constructor(props){
        super();
        this.state = {
           customerid: props.match.params.customerid,
           customername: "",
           customersurname: "",
           customervatid: "",
           sumprice: 0,
           sumextraprice: 0,
           sumpricevat: 0,
           carpetrecords: [],
           cc: [],
           carpettypes: [],
        };
    }

    submitButtonActive(id) {
        const carpet = this.state.cc.find(carpet => carpet.id === id)
        if(carpet === undefined)
            return false
        if(carpet.deliverid == null || carpet.deliverdate == null) 
            return false
         
        return carpet.deliverid.length > 0 && carpet.deliverdate.length > 0
    }



    deliver(id) {
        const carpet = this.state.cc.find(carpetrecord => carpetrecord.id === id);
        deliverCarpet(id, carpet.deliverid, carpet.deliverdate)
        .then(result => {
            this.updateData()
        })
    }

    delete(id) {
        deleteCarpet(id)
        .then(result => {
            this.updateData()
        })
    }

    handleChange(e,id){
        const {carpetrecords,cc} = this.state

        const carpet = carpetrecords.find(carpetrecord => carpetrecord.id === id);
        const carpetIndex = cc.findIndex(carpetrecord => carpetrecord.id === id);
        if(carpetIndex >= 0) {
            if(e.target.id === 'deliverid') {
                cc[carpetIndex].deliverid = e.target.value
            } else if(e.target.id === 'deliverdate') {
                cc[carpetIndex].deliverdate = e.target.value
            }
        } else {
            if(e.target.id === 'deliverid') {
                cc.push({...carpet,deliverid:e.target.value})
            } else if(e.target.id === 'deliverdate') {
                cc.push({...carpet,deliverdate:e.target.value})
            }       
        }
        this.setState({cc:cc})

    }

    updateData() {
        const {cc} = this.state
        getParameters().then(params=>{
            getCarpetsByCustomerId(this.state.customerid).then(carpets=>{
                getCustomerById(this.state.customerid).then(customer=>{
                    this.setState({
                        carpettypes:params,
                        type:params[0],
                        carpetrecords: carpets,
                        cc:cc,
                        sumprice: carpets.filter(carpet=>carpet.deliverid === null)
                        .map(carpet=>carpet.price)
                        .reduce((a, b) => a + b, 0),
                        sumextraprice: carpets.filter(carpet=>carpet.deliverid === null)
                        .map(carpet=>carpet.extraprice)
                        .reduce((a, b) => a + b, 0),
                        sumpricevat: carpets.filter(carpet=>carpet.deliverid === null)
                        .map(carpet=>carpet.pricevat)
                        .reduce((a, b) => a + b, 0),
                        customername: customer.name,
                        customersurname: customer.surname,
                        customervatid: customer.vatid,
                    })
                })
            })
        })
    }

    componentDidMount() {
        this.updateData()
    }


    render() {
        const {carpetrecords, sumprice, sumextraprice, sumpricevat, customername, customersurname, customervatid} = this.state
        return (
            <div>
                <h2>{customername} {customersurname} {customervatid}</h2>
                <Container fluid="xs">
                    <Row key="title">
                        <Col xs={2}><strong>Τύπος</strong></Col>
                        <Col><strong>Κώδ.Χαλιού</strong></Col>
                        <Col><strong>Ράφι</strong></Col>
                        <Col><strong>Αρ.Παραδ</strong></Col>
                        <Col xs={2}><strong>Ημ.Παραδ</strong></Col>
                        <Col><strong>Τιμή</strong></Col>
                        <Col><strong>Έξτρα</strong></Col>
                        <Col><strong>+ΦΠΑ</strong></Col>
                        <Col/>
                        <Col/>
                    </Row>
                    {
                        carpetrecords.map((carpet,index)=>(
                            <Row key={index}>
                                <Col xs={2}>{carpet.typedescription}</Col>
                                <Col><a href={'/carpetinfo/' + carpet.id}>{carpet.id}</a></Col>
                                <Col>{carpet.shelve}</Col>
                              
                                <Col>
                                    {
                                        carpet.deliverid === null ?
                                            <Form.Control id="deliverid" value={carpet.deliverid} onChange={(e) => this.handleChange(e,carpet.id)}/> :
                                            carpet.deliverid
                                    }
                                </Col>
                                <Col xs={2}>
                                    {
                                        carpet.deliverdate === null ?
                                        <Form.Control id="deliverdate" type="date" value={carpet.deliverdate} onChange={(e) => this.handleChange(e,carpet.id)}/> :
                                        dateformat(new Date(carpet.deliverdate), "dd/mm/yyyy")
                                    } 
                                </Col>
                                <Col>{carpet.price.toFixed(2)}</Col>
                                <Col>{carpet.extraprice.toFixed(2)}</Col>
                                <Col>{carpet.pricevat.toFixed(2)}</Col>
                                <Col> 
                                    {
                                        carpet.deliverid === null ? <Button disabled={!this.submitButtonActive(carpet.id)} onClick={this.deliver.bind(this,carpet.id)}>Παράδοση</Button> :
                                        <div/>
                                    }   
                                </Col>
                                <Col><Button onClick={this.delete.bind(this,carpet.id)}>Διαγραφή</Button></Col>
                            </Row> 
                        ))
                    }
                    <Row>
                        <Col xs={2}></Col>
                        <Col/>
                        <Col/>
                        <Col/>
                        <Col xs={2}></Col>
                        <Col><strong>{sumprice.toFixed(2)}</strong></Col>
                        <Col><strong>{sumextraprice.toFixed(2)}</strong></Col>
                        <Col><strong>{sumpricevat.toFixed(2)}</strong></Col>
                        <Col/>
                        <Col/>
                    </Row>
                </Container>
               
            </div>

        )
  }
}

export default CarpetDeliveryTable;
