import axios from "axios";

const base_url = "http://localhost:8080/api/v1"

/*
This is an example snippet - you should consider tailoring it
to your service.

Note: we only handle the first operation here
*/

function fetchGraphQL(
  operationsDoc: string,
  operationName: string,
  variables: Record<string, any>
) {
return fetch('http://localhost:8080/v1/graphql', {
    method: 'POST',
    headers: {
    	'Content-Type': 'application/json',
        'x-hasura-admin-secret': 'myadminsecretkey'
    },
    body: JSON.stringify({
      query: operationsDoc,
      variables,
      operationName,
    }),
  }).then(result => result.json());
}



export const registerCustomer = async (
  surname: string,
  name: string,
  street: string,
  floor: string,
  region: string,
  postalcode: string,
  telephone: string,
  cellphone: string,
  cellphone2: string,
  vatid: string,
  profession: string,
  taxregion: string
  ) => {
  const operation = `
    mutation InsertCustomer {
      insert_customers_one(object: {
        cellphone: ${cellphone},
        cellphone2: ${cellphone2},
        floor:  ${floor}, 
        name:  "${name}",
        profession:  "${profession}",
        postalcode:  ${postalcode},
        surname:  "${surname}",
        street:  "${street}",
        region: "${region}",
        taxregion:  "${taxregion}",
        telephone:  ${telephone},
        vatid:  ${vatid}}) {
        id
      }
    }
  `;
  return fetchGraphQL(operation, "InsertCustomer", {})
}



export const searchCustomer = async (query: string) => {
  return axios
    .get(base_url + "/search/" + query)
    .then((response) => {
      return response.data;
    })
    .catch((err) => {
      console.error(err.message);
      throw err;
    });
};

export const registerCarpet = async(
  customerid: number,
  extent: string,
  extraprice: string,
  length: string,
  notes: string,
  pickupid: number,
  pickupdate: string,
  price: string,
  priceperunit: string,
  pricevat: string,
  typedescription: string,
  weight: string,
  width: string,
  pricetype: string) => {
  return axios.post(base_url + "/carpet/register", {
    priceperunit: priceperunit,
    pricetype: pricetype,
    typedescription: typedescription,
    customerid: customerid,
    length: length,
    width: width,
    weight: weight,
    extent: extent,
    price: price,
    pricevat: pricevat,
    extraprice: extraprice,
    pickupid: pickupid,
    pickupdate: pickupdate,
    notes: notes
  })
  .then((response) => {
    return response.status;
  })
  .catch((err) => {
    console.error(err.message);
    throw err;
  });
}



export const fetchInsertCarpet = async(
  customerid: number,
  deliverdate: string,
  deliverid: number,
  extent: string,
  extraprice: string,
  length: string,
  notes: string,
  pickupid: number,
  pickupdate: string,
  price: string,
  priceperunit: string,
  pricevat: string,
  shelve: string,
  typedescription: string,
  userid: string,
  weight: string,
  width: string,
  pricetype: string) => {
  const operation = `
    mutation InsertCarpet {
      insert_carpets_one(object: {
        customerid: ${customerid},
        deliverdate: ${deliverdate},
        deliverid: ${deliverid},
        extent: ${extent},
        extraprice: ${extraprice},
        length: ${length},
        notes: "${notes}",
        pickupid: ${pickupid},
        pickupdate: "${pickupdate}",
        price: ${price},
        priceperunit: ${priceperunit},
        pricevat: ${pricevat},
        shelve: ${shelve},
        typedescription: ${typedescription},
        userid: ${userid},
        weight: ${weight},
        width: ${width}, 
        pricetype: ${pricetype}}) {
        id
      }
    }
  `;
  return fetchGraphQL(operation, "InsertCarpet", {})
}


export const updateCarpet = async(
  id: number,
    customerid: number,
    deliverdate: string,
    deliverid: number,
    extraprice: string,
    extent: string,
    length: string,
    notes: string,
    pickupdate: string,
    pickupid: number,
    price: string,
    priceperunit: string,
    pricetype: string,
    pricevat: string,
    shelve: string,
    typedescription: string,
    userid: string,
    weight: string,
    width: string) => {
  return axios.post(base_url + "/carpet/update", {
    priceperunit: priceperunit,
    pricetype: pricetype,
    typedescription: typedescription,
    id: id,
    customerid: customerid,
    length: length,
    width: width,
    weight: weight,
    extent: extent,
    price: price,
    pricevat: pricevat,
    extraprice: extraprice,
    notes: notes,
    pickupid: pickupid,
    pickupdate: pickupdate,
    deliverid: deliverid,
    deliverdate: deliverdate,
    shelve: shelve
  })
  .then((response) => {
    return response.status;
  })
  .catch((err) => {
    console.error(err.message);
    throw err;
  });
}




function fetchUpdateCarpet(
  id: number,
    customerid: number,
    deliverdate: string,
    deliverid: number,
    extraprice: string,
    extent: string,
    length: string,
    notes: string,
    pickupdate: string,
    pickupid: number,
    price: string,
    priceperunit: string,
    pricetype: string,
    pricevat: string,
    shelve: string,
    typedescription: string,
    updatedat: string,
    userid: string,
    weight: string,
    width: string) {
  const operation = `
    mutation UpdateCarpet {
      update_carpets_by_pk(
        pk_columns: {id: ${id}}, 
        _set: {
          customerid: ${customerid},
          deliverdate: ${deliverdate},
          deliverid: ${deliverid},
          extraprice: ${extraprice},
          extent: ${extent},
          length: ${length},
          notes: ${notes},
          pickupdate: ${pickupdate},
          pickupid: ${pickupid},
          price: ${price},
          priceperunit: ${priceperunit},
          pricetype: ${pricetype},
          pricevat: ${pricevat},
          shelve: ${shelve},
          typedescription: ${typedescription},
          updatedat: ${updatedat},
          userid: ${userid},
          weight: ${weight},
          width: ${width}}) {
        id
      }
    }
  `;
  return fetchGraphQL(operation, "UpdateCarpet", {})
}

export const deliverCarpet2 = async(id: string, deliverid: number, deliverdate: string) => {
  const operation = `
    mutation UpdateCarpet {
      update_carpets_by_pk(
        pk_columns: {id: ${id}}, 
        _set: {
          deliverdate: ${deliverdate},
          deliverid: ${deliverid}
        id
      }
    }
  `;
  return fetchGraphQL(operation, "UpdateCarpet", {})
}

export const deliverCarpet = async(id: string, deliverid: number, deliverdate: string) => {
  return axios.post(base_url + "/carpet/deliver/" + id, {
    deliverid: deliverid,
    deliverdate: deliverdate
  })
  .then((response) => {
    return response.status;
  })
  .catch((err) => {
    console.error(err.message);
    throw err;
  });
}


export const getCarpetsByCustomerId2 = async (customerid: number) => {
  
const operation = `
query GetCarpetsByCustomerById {
  customers_by_pk(id: ${customerid}) {
    carpets {
      createdat
      customerid
      deliverdate
      deliverid
      extent
      extraprice
      id
      length
      notes
      width
      weight
      updatedat
      typedescription
      shelve
      pricevat
      pricetype
      priceperunit
      price
      pickupid
      pickupdate
    }
  }
}
`;

return fetchGraphQL(operation, "GetCarpetsByCustomerById", {});

}

export const getCarpetsByCustomerId = async (customerid: number) => {
  return axios.get(base_url + "/carpet/getbycustomer/" + customerid)
  .then((response) => {
    return response.data;
  })
  .catch((err) => {
    console.error(err.message);
    throw err;
  });
}


export const getCarpetById2 = async (carpetid: number) => {
  const operation = `
    query GetCarpetById {
      carpets_by_pk(id: ${carpetid}) {
        customerid
        createdat
        deliverdate
        deliverid
        extent
        extraprice
        id
        length
        customer {
          cellphone
          cellphone2
          floor
          createdat
          name
          id
          postalcode
          region
          profession
          street
          taxregion
          surname
          telephone
          updatedat
          vatid
        }
        notes
        pickupid
        pickupdate
        priceperunit
        price
        pricevat
        pricetype
        shelve
        typedescription
        updatedat
        weight
        width
      }
    }
  `;
  return fetchGraphQL(operation, "GetCarpetById", {});
}

export const getCarpetById = async (carpetid: number) => {
  return axios.get(base_url + "/carpet/getbyid/" + carpetid)
  .then((response) => {
    return response.data;
  })
  .catch((err) => {
    console.error(err.message);
    throw err;
  });
}


export const getCarpetsByShelveId2 = async (shelveid: number) => {
  const operation = `
    query GetCarpetsByShelveId {
      carpets(where: {shelve: {_eq: "${shelveid}"}}) {
        customerid
        deliverdate
        deliverid
        extent
        id
        extraprice
        length
        notes
        pickupdate
        price
        pickupid
        priceperunit
        pricevat
        pricetype
        shelve
        typedescription
        updatedat
        width
        weight
        createdat
        customer {
          cellphone
          cellphone2
          createdat
          floor
          name
          postalcode
          profession
          surname
          telephone
          updatedat
          vatid
          street
          taxregion
          region
          id
        }
      }
    }
  `;

  return fetchGraphQL(operation, "GetCarpetsByShelveId", {});


}


export const getCarpetsByShelveId = async (shelveid: number) => {
  return axios.get(base_url + "/shelve/get/" + shelveid)
  .then((response) => {
    return response.data;
  })
  .catch((err) => {
    console.error(err.message);
    throw err;
  });
}

export const registerParameter2 = async (description: string, type: string, price: number)=> {
  const createdat = (new Date()).getTime();

  const operation = `
    mutation InsertParameter {
      insert_parameters_one(object: {createdat: "${createdat}", description: "${description}", type: "${type}", updatedat: "${createdat}", price: ${price}})
    }
  `;

  return fetchGraphQL(operation, "InsertParameter", {});
}

export const registerParameter = async (description: string, type: string, price: number)=> {
  return axios.post(base_url + "/parameter/register", {
    description: description,
    type: type,
    price: price
  })
  .then((response) => {
    return response.status;
  })
  .catch((err) => {
    console.error(err.message);
    throw err;
  });
}


export const deleteParameter2  = async (paramid: number)=> {
  const operation = `
    mutation DeleteParameterByID {
      delete_parameters_by_pk(id: 10) {
        id
      }
    }
  `;

  return fetchGraphQL(operation, "DeleteParameterByID", {});
}

export const deleteParameter  = async (paramid: number)=> {
  return axios.post(base_url + "/parameter/delete/" + paramid)
  .then((response) => {
    return response.status;
  })
  .catch((err) => {
    console.error(err.message);
    throw err;
  });
}


export const getParameters2 = async ()=> {
  const operation = `
    query GetParameters {
      parameters {
        createdat
        description
        id
        price
        type
        updatedat
      }
    }
  `;

  return fetchGraphQL(operation, "GetParameters", {});
}


export const getParameters = async ()=> {
  return axios.get(base_url + "/parameter/get")
  .then((response) => {
    return response.data;
  })
  .catch((err) => {
    console.error(err.message);
    throw err;
  });
}



export const getCustomerById2 = async (id: number)=> {
  const operation = `
    query GetCustomerById {
      customers_by_pk(id: ${id}) {
        cellphone
        cellphone2
        createdat
        floor
        id
        profession
        street
        region
        postalcode
        name
        surname
        taxregion
        telephone
        updatedat
        vatid
      }
    }
  `;

  return fetchGraphQL(operation, "GetCustomerById", {});
}

export const getCustomerById = async (id: number)=> {
  return axios.get(base_url + "/customer/get/" + id)
  .then((response) => {
    return response.data;
  })
  .catch((err) => {
    console.error(err.message);
    throw err;
  });
}

export const getVat = async ()=> {
  return axios.get(base_url + "/vat")
  .then((response) => {
    return response.data;
  })
  .catch((err) => {
    console.error(err.message);
    throw err;
  });
}

export const setVat = async(vat: string)=> {
  return axios.post(base_url + "/vat", {'vat': parseInt(vat)})
  .then((response) => {
    return response.status;
  })
  .catch((err) => {
    console.error(err.message);
    throw err;
  });
}

export const getCustomers2 = async () => {
  const operation = `
    query GetCustomers {
      customers {
        cellphone
        cellphone2
        createdat
        floor
        id
        name
        postalcode
        profession
        region
        street
        surname
        taxregion
        telephone
        updatedat
        vatid
      }
    }
  `;

  return fetchGraphQL(operation, "GetCustomers", {});
}


export const getCustomers = async ()=> {
  return axios.get(base_url + "/customers")
  .then((response) => {
    return response.data;
  })
  .catch((err) => {
    console.error(err.message);
    throw err;
  });
}


export const deleteCustomer2 = async (id: number)=> {
  const operation = `
    mutation DeleteCustomerById {
      delete_customers_by_pk(id: ${id}) {
        id
      }
    }
  `;

  return fetchGraphQL(operation, "DeleteCustomerById", {});
}


export const deleteCustomer = async (id: number)=> {
  return axios.post(base_url + "/customer/delete/" + id)
  .then((response) => {
    return response.data;
  })
  .catch((err) => {
    console.error(err.message);
    throw err;
  });
}


export const deleteCarpet2 = async (id: number)=> {
  const operation = `
    mutation DeleteCarpetById {
      delete_carpets_by_pk(id: ${id}) {
        id
      }
    }
  `;

  return fetchGraphQL(operation, "DeleteCarpetById", {});
}

export const deleteCarpet = async (id: number)=> {
  return axios.post(base_url + "/carpet/delete/" + id)
  .then((response) => {
    return response.data;
  })
  .catch((err) => {
    console.error(err.message);
    throw err;
  });
}


export const getYearReport = async() => {
  const currentYear = new Date().getFullYear(); 
  return axios.get(base_url + "/report/" + currentYear)
  .then((response) => {
    return response.data
  })
  .catch((err) => {
    console.error(err.message);
    throw err;
  });
}
