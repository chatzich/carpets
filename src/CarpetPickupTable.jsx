import React from 'react'
import { Row, Col, Container, Button, Form } from 'react-bootstrap';
import { registerCarpet, getCarpetsByCustomerId, getParameters, getVat, getCustomerById } from './UtilityFunctions'
import dateformat from "dateformat";
import ServerMessage from './ServerMessage'

class CarpetPickupTable extends React.Component {
    constructor(props){
        super();
        this.state = {
           id: "",
           typedescription: "",
           vat: 24,
           notes: "",
           pickupid: "",
           pickupdate: "",
           customerid: props.match.params.customerid,
           customername: "",
           customersurname: "",
           customervatid: "",
           carpetrecords: [],
           carpettypes: [],
           status:0
        };
        this.shelvechars = ['Α','Β','Γ','Δ','Ε','Ζ','Η','Θ','Ζ','Ι','Κ','Λ','Μ','Ν','Ξ','Ο','Π','Ρ','Σ','Τ','Υ','Φ','Χ','Ψ','Ω']
    }


    submitButtonActive() {
        return this.state.typedescription.length > 0 && 
        this.state.pickupid.length > 0 &&
        this.state.pickupdate.length > 0;
    }

    register() {
        const unitprice = this.getCurrentTypePrice(this.state.typedescription)
        registerCarpet(unitprice, this.getCurrentType(this.state.typedescription),
            this.state.typedescription, this.state.id, this.state.customerid, 
            1, 1,
            1, 1,
            unitprice, 0, unitprice * (1 + this.state.vat/100),
            this.state.pickupid, this.state.pickupdate, this.state.notes)
        .then(result => {
            getCarpetsByCustomerId(this.state.customerid).then(carpets=>{
                this.setState({carpetrecords: carpets})
            })
        }).catch(error=>{
            this.setState({
                status:403
            })
            console.log("Result of pickup:" + JSON.stringify(error, null, 2))
        })
    }

    handleChange(e){
        this.setState({
            [e.target.id]: e.target.value,
        })
    }

    componentDidMount() {
        
        getParameters().then(params=>{
            this.setState({
                carpettypes:params
            })
            getCarpetsByCustomerId(this.state.customerid).then(carpets=>{
                getVat().then(vat=>{
                    this.setState({
                        carpetrecords: carpets,
                        typedescription: params.length > 0 ? params[0].description : "",
                        vat:vat.value
                    })
                    getCustomerById(this.state.customerid).then(customer=>{
                        this.setState({
                            customername: customer.name,
                            customersurname: customer.surname,
                            customervatid: customer.vatid,
                        })
                    })
                })
            })
        })
    }

    getCurrentType(typedescription) {
        const type = this.state.carpettypes.find(carpettype=>carpettype.description===typedescription)
        if(type === undefined)
            return this.state.carpettypes[0].type;
        return type.type
    }

  
    getCurrentTypePrice(typedescription) {
        const type = this.state.carpettypes.find(carpettype=>carpettype.description===typedescription)
        if(type === undefined)
            return this.state.carpettypes[0].price;
        return type.price
    }

    
    render() {
        const {carpettypes, carpetrecords, customername, customersurname, customervatid} = this.state

        return (
            <div>
                <h2>{customername} {customersurname} {customervatid}</h2>

                <Container fluid="xs">
                    <Row key="title">
                        <Col><strong>Κώδ.Χαλιού</strong></Col>

                        <Col><strong>Τύπος</strong></Col>
                        <Col><strong>Παρατηρήσεις</strong></Col>
                        <Col><strong>Αρ.Παραλ</strong></Col>
                        <Col xs={2}><strong>Ημ.Παραλ</strong></Col>
                    </Row>
                    {
                        carpetrecords.map((carpet,index)=>(
                            <Row key={index}>
                                <Col><a href={'/carpetinfo/' + carpet.id}>{carpet.id}</a></Col>
                                <Col>{carpet.typedescription}</Col>
                                <Col>{carpet.notes}</Col>
                                <Col>{carpet.pickupid}</Col>
                                <Col xs={2}>{dateformat(new Date(carpet.pickupdate),  "dd/mm/yyyy")}</Col>
                            </Row>
                        ))
                    }
                    <Row>
                        <Col><Form.Control id="id" type="number" step={1} min={0} value={this.state.id} onChange={(e) => this.handleChange(e)}/></Col>
                        <Col>
                            <Form.Control id="typedescription" value={this.state.typedescription} onChange={(e) => this.handleChange(e)} as="select" custom>
                            {
                                carpettypes.map((carpettype,index)=>(
                                    <option key={index}>{carpettype.description}</option>
                                ))
                            }
                            </Form.Control>
                        </Col>
                        <Col><Form.Control id="notes" value={this.state.notes} onChange={(e) => this.handleChange(e)}/></Col>
                        <Col><Form.Control id="pickupid" value={this.state.pickupid} onChange={(e) => this.handleChange(e)}/></Col>
                        <Col xs={2}><Form.Control id="pickupdate" type="date" value={this.state.pickupdate} onChange={(e) => this.handleChange(e)}/></Col>
                    </Row>
                </Container>
                <Button style={{
                   float: "right",
                   margin: "15px"
                 }}
                 onClick={()=>{this.register()}}
                 disabled={!this.submitButtonActive()}
                 >Καταχώρηση</Button>
                <ServerMessage redirect={'/carpetpickup/' + this.state.customerid} show={this.state.status === 403} title='Λάθος!' message='Το προιόν δεν μπόρεσε να καταχωρηθεί'/>

            </div>

        )
  }
}

export default CarpetPickupTable;
