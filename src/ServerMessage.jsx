import React, { Component } from "react";
import { Modal, Button } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";

class ServerMessage extends Component {

  render() {
    const { title, message, redirect, show } = this.props
    return (
      <Modal show={show}>
        <Modal.Header closeButton>
          <Modal.Title>{title}</Modal.Title>
        </Modal.Header>
        <Modal.Body>{message}</Modal.Body>
        <Modal.Footer>
          <Button href={redirect} variant="primary" >
          OK
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

export default ServerMessage;
