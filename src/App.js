import React from 'react';
import './App.css';
import Customer from './Customer';
import CarpetPickupTable from './CarpetPickupTable';
import CarpetDeliveryTable from './CarpetDeliveryTable';
import NavComp from './NavComp';
import Parameters from './Parameters';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import CustomerTable from './CustomerTable';
import ShelveTable from './ShelveTable'
import Shelve from './Shelve'
import Info from './Info'
import CarpetInfo from './CarpetInfo'

function App() {

  return (
    <div className="App">
      <Router>
        <NavComp />
       <Switch>
       <Route exact path="/" component={Info}/>

        <Route exact path="/newcustomer" component={Customer}/>
        <Route exact path="/carpetpickup/:customerid" component={CarpetPickupTable}/>
        <Route exact path="/carpetdelivery/:customerid" component={CarpetDeliveryTable}/>
        <Route exact path="/carpetinfo/:carpetid" component={CarpetInfo}/>
        <Route exact path="/customerlist" component={CarpetInfo}/>

        <Route exact path="/shelvetable" component={ShelveTable}/>
        <Route exact path="/shelve/:shelveid" component={Shelve}/>

        <Route exact path="/search/:query" component={CustomerTable}/>
        <Route exact path="/customers" component={CustomerTable}/>

        <Route exact path="/parameters" component={Parameters}/>

       </Switch>
      </Router>

    </div>
  );
}

export default App;
