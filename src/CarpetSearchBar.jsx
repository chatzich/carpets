import React, { Component } from "react";
import { Form, FormControl, Button } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";

class CarpetSearchBar extends Component {
  constructor() {
    super();
    this.state = {
      carpetid: 0,
    };
  }

  handleIdChange = (event) => {
    this.setState({
      carpetid: event.target.value
    });
  };

  render() {
    return (
      <div>
        <Form inline>
          <FormControl
            type="number"
            placeholder="Αναζήτηση Προϊόντος"
            onChange={this.handleIdChange.bind(this)}
          />
          <Button
            style={{margin: "2px"}}
            href={"/carpetinfo/" + this.state.carpetid}
          >
            Αναζήτηση
          </Button>
        </Form>
      </div>
    );
  }
}

export default CarpetSearchBar;
