import React from 'react'
import { Row, Col, Container } from 'react-bootstrap';
import { getCarpetsByShelveId} from './UtilityFunctions'
import dateformat from "dateformat";

class ShelveTable extends React.Component {
    constructor(props){
        super();
        this.state = {
            shelveid: props.match.params.shelveid,
            carpets: []
        }
    }
    componentDidMount() {
        getCarpetsByShelveId(this.state.shelveid).then(carpets=>{
            this.setState({carpets: carpets.filter(carpet=>carpet.deliverid === null)})
        })
    }


    render() {
        const {carpets, shelveid} = this.state
        return (
            <div>
                <h2>Ράφι {shelveid}</h2>
                <Container fluid>
                    <Row>
                        <Col xs={2}><strong>Τύπος</strong></Col>
                        <Col><strong>ΑΑ</strong></Col>
                        <Col><strong>Τιμή</strong></Col>
                        <Col><strong>Έξτρα</strong></Col>
                        <Col><strong>+ΦΠΑ</strong></Col>
                        <Col><strong>Ράφι</strong></Col>
                        <Col><strong>Αρ.Παραλ</strong></Col>
                        <Col xs={2}><strong>Ημ.Παραλ</strong></Col>
                    </Row>
                    {
                        carpets.map(carpet=>(
                            <Row>
                                <Col xs={2}>{carpet.typedescription}</Col>
                                <Col><a href={'/carpetinfo/' + carpet.id}>{carpet.id}</a></Col>
                                <Col>{carpet.price.toFixed(2)}</Col>
                                <Col>{carpet.extraprice.toFixed(2)}</Col>
                                <Col>{carpet.pricevat.toFixed(2)}</Col>
                                <Col>{carpet.shelve}</Col>
                                <Col>{carpet.pickupid}</Col>
                                <Col xs={2}>{dateformat(new Date(carpet.pickupdate))}</Col>
                            </Row>
                        ))
                    }
                </Container>
            </div>

        )
  }
}

export default ShelveTable;
