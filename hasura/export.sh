hasura migrate create "init" --from-server --admin-secret myadminsecretkey
hasura metadata export --admin-secret myadminsecretkey
sqlacodegen postgresql://postgres:postgrespassword@localhost:5432/carpets
